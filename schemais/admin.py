# -*- coding: utf-8 -*- 
from django.contrib import admin
from django.template.defaultfilters import slugify
from models import *
#from django.conf import settings
import reversion
from treeadmin.admin import TreeAdmin

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.generic import GenericTabularInline



def create_action(agenda):
	def action(modeladmin, request, queryset):
		rows_updated = queryset.update(parent=agenda)
		if rows_updated == 1:
			message = u'Jedna položka byla úspěšně přesunuta'
		elif rows_updated in (2,3,4):
			message = u'%s položky byly úspěšně přesunuty' % rows_updated
		else:
			message = u'%s položek bylo úspěšně přesunuto' % rows_updated
		modeladmin.message_user(request, message)
	name = 'move_%s' % (slugify(agenda.name),)
	return (name, (action, name, u'Přesunout vybrané položky do agendy "%s"' % (agenda,)))



class AgendaISListfilter(admin.SimpleListFilter):
	title = u'IS nebo jeho části'
	parameter_name = 'isfilter'
	template = 'admin/filter_selectbox.html'

	def lookups(self, request, model_admin):
		ret = []
		for i in InformationSystem.objects.all():
			ret.append(('is_%s' % i.id, i.name))
			for a in i.application_set.all():
				ret.append(('app_%s' % a.id, u'--'+a.name))
				for p in a.partis_set.all():
					ret.append(('part_%s' % p.id, u'----'+p.name))
		return ret

	def queryset(self, request, queryset):
		if self.value():
			typ, id = self.value().split('_')
			if typ == 'is':
				return queryset.filter(partis__application__information_system=id).distinct()
			elif typ == 'app':
				return queryset.filter(partis__application=id).distinct()
			elif typ == 'part':
				return queryset.filter(partis=id)
			return queryset


class AgendaTreeListFilter(admin.SimpleListFilter):
	title = u'Agendy'
	parameter_name = 'agenda'
	template = 'admin/filter_selectbox.html'

	def lookups(self, request, model_admin):
		ret = []
		for a in Agenda.objects.all():
			ret.append((a.id, u'---'*a.level + u' ' + a.name))
		return ret

	def queryset(self, request, queryset):
		if self.value():
			return queryset.filter(agenda=self.value())
#		return queryset



class AgendaTreeParentFilter(AgendaTreeListFilter):
	title = u'Zastřešující agendy'
	parameter_name = 'parent_agenda'

	def queryset(self, request, queryset):
		if self.value():
			a = Agenda.objects.get(id=self.value())
			d = a.get_descendants(True)
			return queryset.filter(id__in=d)



class AgendaLegislationFilter(admin.SimpleListFilter):
	title = u'agendy'
	parameter_name = 'agendalegislation'

	def lookups(self, request, model_admin):
		ret = [('None', u' -- Bez přiřazené agendy --'), ('AgendaOnly', u' -- Pouze s přiřazenými agendami --')]
		for a in Agenda.objects.all():
			ret.append((a.id, u'---'*a.level + u' ' + a.name))
		return ret

	def queryset(self, request, queryset):
		value = self.value()
		if value == 'None':
			return queryset.filter(agenda=None)
		elif value == 'AgendaOnly':
			return queryset.exclude(agenda=None)
		elif value:
			return queryset.filter(agenda=value)
#		return queryset



#class SelectFilter(admin.filters.SimpleListFilter):
#class SelectFilter(admin.filters.AllValuesFieldListFilter):
#class SelectFilter(admin.filters.FieldListFilter):
class RelatedSelectFilter(admin.filters.RelatedFieldListFilter):
	template = 'admin/filter_selectbox.html'

class ChoicesSelectFilter(admin.filters.ChoicesFieldListFilter):
	template = 'admin/filter_selectbox.html'

class DateSelectFilter(admin.filters.DateFieldListFilter):
	template = 'admin/filter_selectbox.html'

class AllSelectFilter(admin.filters.AllValuesFieldListFilter):
	template = 'admin/filter_selectbox.html'



ISfieldsets = (
	(None, {
		'fields': (
			('name', 'code'), ('parent', 'operated',), ('date_from', 'date_to', 'color'), 'description', ('administrator', 'app_administrator', 'business_owner'), ('access', 'status', 'license'), 'agenda',
		)
	}),
	(u'Umístění', {
		'fields': (
			'location', 'location_url',
		),
		'classes': ('collapse',),
	}),
	(u'Dokumentace', {
		'fields': (
			'documentation', 'documentation_url',
		),
		'classes': ('collapse',),
	}),
	(u'Architektura', {
		'fields': (
			'architecture_os', 'architecture_as', 'architecture_db',
		),
		'classes': ('collapse',),
	}),
	(u'Zálohování', {
		'fields': (
			'backup_app', 'backup_data', 'backup_file',
		),
		'classes': ('collapse',),
	}),
)

APPfieldsets = (
	(None, {
		'fields': (
			('name', 'code', 'operated'), ('information_system', 'previous'), ('date_from', 'date_to',), 'description', ('administrator', 'access'), ('status', 'license'),
		)
	}),
	(u'Umístění', {
		'fields': (
			'location', 'location_url',
		),
		'classes': ('collapse',),
	}),
	(u'Dokumentace', {
		'fields': (
			'documentation', 'documentation_url',
		),
		'classes': ('collapse',),
	}),
	(u'Architektura', {
		'fields': (
			'architecture_os', 'architecture_as', 'architecture_db',
		),
		'classes': ('collapse',),
	}),
	(u'Zálohování', {
		'fields': (
			'backup_app', 'backup_data', 'backup_file',
		),
		'classes': ('collapse',),
	}),
)

PARTISfieldsets = (
	(None, {
		'fields': (
			('name', 'code', 'operated'), ('application', 'agenda', 'previous'), ('date_from', 'date_to',), 'description', ('administrator', 'access'), ('status', 'license'), 'note', 'improvement'
		)
	}),
	(u'Umístění', {
		'fields': (
			'location', 'location_url',
		),
		'classes': ('collapse',),
	}),
	(u'Dokumentace', {
		'fields': (
			'documentation', 'documentation_url',
		),
		'classes': ('collapse',),
	}),
	(u'Architektura', {
		'fields': (
			'architecture_os', 'architecture_as', 'architecture_db',
		),
		'classes': ('collapse',),
	}),
	(u'Zálohování', {
		'fields': (
			'backup_app', 'backup_data', 'backup_file',
		),
		'classes': ('collapse',),
	}),
)

#class PricingInline(admin.TabularInline):
#	model = Pricing

#class PartISInline(admin.StackedInline):
#	model = PartIS
#	fieldsets = PARTISfieldsets
#	class Media:
#		js = ['schemais/js/collapsed_stacked_inlines.js',]

#class ApplicationInline(admin.StackedInline):
#	model = Application
#	fieldsets = APPfieldsets
#	class Media:
#		js = ['schemais/js/collapsed_stacked_inlines.js',]

class UsersInline(admin.TabularInline):
	model = Users

class LegislationAdmin(reversion.VersionAdmin):
	fieldsets = (
		(None, {
			'fields': (
				'name',
				('status', 'date_from'),
				'description',
			)
		}),
	)
	list_display = ['name', 'status', 'agenda_links', 'date_from']
	list_filter = ['status', AgendaLegislationFilter]
	search_fields = ['name', 'description']
	date_hierarchy = 'date_from'
	save_as = True
	save_on_top = True



class AgendaAdmin(TreeAdmin, reversion.VersionAdmin):
	fieldsets = (
		(None, {
			'fields': (
#				('name', 'ordering'),
				('name', 'date_from'),
				('parent', 'color'),
				'legislation',
#				('date_from', 'status'),
				'description',
				'reason',
				'requirement',
				'comm_ext',
				'comm_int',
				'users_note',
				'note',
			)
		}),
	)
	#list_display = ['name', 'legislation_link', 'is_links', 'date_from', 'show_color', 'color']
#	list_display = ['name', 'legislation_link', 'is_links', 'date_from', 'show_color']
	list_display = ['name', 'date_from', 'show_color']
	list_filter = (
		'legislation', 'parent',
#		('legislation', RelatedSelectFilter),
#		('parent', RelatedSelectFilter),
#		AgendaTreeParentFilter,
#		AgendaISListfilter,
	)
	filter_horizontal = ('legislation',)
	search_fields = ['name', 'description', 'reason', 'requirement', 'comm_ext', 'comm_int', 'note',]
	date_hierarchy = 'date_from'
#	list_editable = ['parent', 'legislation']
#	list_editable = ['color']
#	raw_id_fields = ['parent']
	save_as = True
	save_on_top = True
	ordering = ['name']

	def get_actions(self, request):
		actions = dict(create_action(q) for q in Agenda.objects.filter(parent=None).order_by('name'))
		# doplneni puvodnich actions
		actions_orig = super(AgendaAdmin, self).get_actions(request)
		for action in actions_orig:
			actions[action] = actions_orig[action]
		return actions



#class AgendaRelationAdmin(reversion.VersionAdmin):
#	fieldsets = (
#		(None, {
#			'fields': (
#				('source', 'destination'),
#				'description',
#			)
#		}),
#	)
#	list_display = ['source', 'destination',]
#	list_filter = ['source', 'destination']
#	search_fields = ['description']
#
#class PricingAdmin(reversion.VersionAdmin):
#	fieldsets = (
#		(None, {
#			'fields': (
#				'license', ('once', 'period',), 'note'
#			)
#		}),
#	)
#	list_display = ['license', 'once', 'period']

class InformationSystemAdmin(TreeAdmin, reversion.VersionAdmin):
	fieldsets = ISfieldsets
	list_display = ['name', 'code', 'administrator', 'status', 'date_from', 'operated', 'show_color']
	list_filter = (('code', AllSelectFilter), ('status', ChoicesSelectFilter),'access', 'operated', ('license', RelatedSelectFilter), 'architecture_os', 'architecture_as', 'architecture_db')
	filter_horizontal = ('agenda',)
	search_fields = ['name', 'description', 'administrator', 'location', 'documentation', ]
#	list_editable = ['code', 'administrator', 'status', 'operated']
	inlines = [UsersInline]
	save_as = True
	save_on_top = True
	list_max_show_all = 1000
	list_per_page = 1000

#class ApplicationAdmin(reversion.VersionAdmin):
#	fieldsets = APPfieldsets
#	list_display = ['name', 'code', 'information_system', 'date_from', 'status', 'operated']
#	list_filter = [('information_system', RelatedSelectFilter), ('information_system__code'), ('status', ChoicesSelectFilter), 'access', ('license', RelatedSelectFilter), 'architecture_os', 'architecture_as', 'architecture_db']
##	list_filter = [('information_system', RelatedSelectFilter), ('information_system__code', AllSelectFilter), ('status', ChoicesSelectFilter), 'access', ('license', RelatedSelectFilter), 'architecture_os', 'architecture_as', 'architecture_db']
#	search_fields = ['name', 'description', 'administrator', 'location', 'documentation', ]
##	list_editable = ['date_from', 'status', 'operated']
#	inlines = [PartISInline]
#	list_per_page =  30
#	save_as = True
#	save_on_top = True
#
#class PartISAdmin(reversion.VersionAdmin):
#	fieldsets = PARTISfieldsets
#	list_display = ['name', 'code', 'application', 'agenda_link', 'date_from', 'status', 'operated']
#	list_filter = [
##		('application__information_system__code', AllSelectFilter), ('application__code', AllSelectFilter), ('agenda', RelatedSelectFilter), ('application__information_system', RelatedSelectFilter),
##		('application__information_system__code'), ('application__code'), ('agenda', RelatedSelectFilter), ('application__information_system'),
#		('application__information_system__code'), ('application__code'), (AgendaTreeListFilter), ('application__information_system'),
##		('application', RelatedSelectFilter), ('status', ChoicesSelectFilter), 'access', ('license', RelatedSelectFilter), 'architecture_os', 'architecture_as', 'architecture_db']
#		('application'), ('status', ChoicesSelectFilter), 'operated', 'access', ('license', RelatedSelectFilter), 'architecture_os', 'architecture_as', 'architecture_db']
#	search_fields = ['name', 'description', 'administrator', 'location', 'documentation', 'improvement']
##	list_editable = ['date_from', 'status', 'operated']
#	list_per_page =  30
#	save_as = True
#	save_on_top = True

class LinkAdmin(reversion.VersionAdmin):
	list_display = ['__unicode__', 'note', 'typ_status', 'frequency_status', 'have_improvement']
	list_filter = [('source', RelatedSelectFilter), ('destination', RelatedSelectFilter), 'typ', 'frequency', ]
	search_fields = ['note', 'description', 'improvement']


#class GenericCollectionInlineModelAdmin(admin.options.InlineModelAdmin):
#	ct_field = "content_type"
#	ct_fk_field = "object_id"
##	ct_fields = ['source_content_type', 'destination_content_type']
##	ct_fk_fields = ['source_object_id', 'destination_object_id']
#	def __init__(self, parent_model, admin_site):
#		super(GenericCollectionInlineModelAdmin, self).__init__(parent_model, admin_site)
#		ctypes = ContentType.objects.all().order_by('id').values_list('id', 'app_label','model')
#		elements = ["%s: '%s/%s'" % (id, app_label, model) for id, app_label, model in ctypes]
#		self.content_types = "{%s}" % ",".join(elements)
#
#	def get_formset(self, request, obj=None, **kwargs):
#		result = super(GenericCollectionInlineModelAdmin, self).get_formset(request, obj, **kwargs)
#		result.content_types = self.content_types
#		result.ct_fk_field = self.ct_fk_field
##		result.ct_fk_fields = self.ct_fk_fields
#		return result

#class GenericCollectionTabularInline(GenericCollectionInlineModelAdmin):
#	template = 'admin/edit_inline/gen_coll_tabular.html'

#class GenericCollectionStackedInline(GenericCollectionInlineModelAdmin):
#	template = 'admin/edit_inline/gen_coll_stacked.html'


from genericadmin.admin import GenericAdminModelAdmin, TabularInlineWithGeneric
#class ProblemRelationInline(admin.TabularInline):
class ProblemRelationInline(TabularInlineWithGeneric):
#class ProblemRelationInline(GenericCollectionTabularInline):
	model = ProblemRelation
	related_lookup_fields = {
		'generic': [['content_type', 'object_id'],]
	}
	extra = 3

class ProblemAdmin(reversion.VersionAdmin, GenericAdminModelAdmin):
#class ProblemAdmin(reversion.VersionAdmin):
	list_display = ['name']
	search_fields = ['name', 'description']
	inlines = [ProblemRelationInline]

#	class Media:
#		js = ('schemais/js/genericcollection.js',)



admin.site.register(Legislation, LegislationAdmin)
admin.site.register(Agenda, AgendaAdmin)
#admin.site.register(AgendaRelation, AgendaRelationAdmin)
#admin.site.register(Pricing, PricingAdmin)
admin.site.register(InformationSystem, InformationSystemAdmin)
#admin.site.register(Application, ApplicationAdmin)
#admin.site.register(PartIS, PartISAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(Problem, ProblemAdmin)

class Admin(reversion.VersionAdmin):
	fieldsets = (
		(None, {
			'fields': (
			)
		}),
	)

