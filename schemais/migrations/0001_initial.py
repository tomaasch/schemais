# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Agenda'
        db.create_table(u'schemais_agenda', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ordering', self.gf('django.db.models.fields.PositiveIntegerField')(blank=True)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('status', self.gf('django.db.models.fields.PositiveIntegerField')(blank=True)),
            ('reason', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('requirement', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('comm_ext', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('comm_int', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('start', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('note', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['Agenda'])

        # Adding model 'AgendaRelation'
        db.create_table(u'schemais_agendarelation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sources_set', to=orm['schemais.Agenda'])),
            ('destination', self.gf('django.db.models.fields.related.ForeignKey')(related_name='destinations_set', to=orm['schemais.Agenda'])),
            ('typ', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['AgendaRelation'])

        # Adding model 'License'
        db.create_table(u'schemais_license', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['License'])

        # Adding model 'Pricing'
        db.create_table(u'schemais_pricing', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('license', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.License'])),
            ('once', self.gf('django.db.models.fields.IntegerField')()),
            ('period', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'schemais', ['Pricing'])

        # Adding model 'OperatingSystem'
        db.create_table(u'schemais_operatingsystem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['OperatingSystem'])

        # Adding model 'AplicationServer'
        db.create_table(u'schemais_aplicationserver', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['AplicationServer'])

        # Adding model 'Database'
        db.create_table(u'schemais_database', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['Database'])

        # Adding model 'Department'
        db.create_table(u'schemais_department', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
        ))
        db.send_create_signal(u'schemais', ['Department'])

        # Adding model 'InformationSystem'
        db.create_table(u'schemais_informationsystem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('location', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('location_url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('documentation', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('documentation_url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('date_from', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_to', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.PositiveIntegerField')(blank=True)),
            ('administrator', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('license', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.License'])),
            ('architecture_os', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.OperatingSystem'])),
            ('architecture_as', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.AplicationServer'])),
            ('architecture_db', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.Database'])),
            ('backup_app', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('backup_data', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('backup_file', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('access', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'schemais', ['InformationSystem'])

        # Adding model 'PartIS'
        db.create_table(u'schemais_partis', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('information_system', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.InformationSystem'])),
            ('description', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('location', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('location_url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('documentation', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('documentation_url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('previous', self.gf('django.db.models.fields.related.ForeignKey')(related_name='previous_part_set', to=orm['schemais.PartIS'])),
            ('date_from', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_to', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.PositiveIntegerField')(blank=True)),
            ('agenda', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.Agenda'])),
            ('administrator', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('note', self.gf('tinymce.models.HTMLField')(blank=True)),
            ('license', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.License'])),
            ('architecture_os', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.OperatingSystem'])),
            ('architecture_as', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.AplicationServer'])),
            ('architecture_db', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schemais.Database'])),
            ('backup_app', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('backup_data', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('backup_file', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('access', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'schemais', ['PartIS'])


    def backwards(self, orm):
        # Deleting model 'Agenda'
        db.delete_table(u'schemais_agenda')

        # Deleting model 'AgendaRelation'
        db.delete_table(u'schemais_agendarelation')

        # Deleting model 'License'
        db.delete_table(u'schemais_license')

        # Deleting model 'Pricing'
        db.delete_table(u'schemais_pricing')

        # Deleting model 'OperatingSystem'
        db.delete_table(u'schemais_operatingsystem')

        # Deleting model 'AplicationServer'
        db.delete_table(u'schemais_aplicationserver')

        # Deleting model 'Database'
        db.delete_table(u'schemais_database')

        # Deleting model 'Department'
        db.delete_table(u'schemais_department')

        # Deleting model 'InformationSystem'
        db.delete_table(u'schemais_informationsystem')

        # Deleting model 'PartIS'
        db.delete_table(u'schemais_partis')


    models = {
        u'schemais.agenda': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Agenda'},
            'comm_ext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'comm_int': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'ordering': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'}),
            'reason': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'requirement': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'})
        },
        u'schemais.agendarelation': {
            'Meta': {'object_name': 'AgendaRelation'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'destinations_set'", 'to': u"orm['schemais.Agenda']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources_set'", 'to': u"orm['schemais.Agenda']"}),
            'typ': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'schemais.aplicationserver': {
            'Meta': {'ordering': "['name']", 'object_name': 'AplicationServer'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.database': {
            'Meta': {'ordering': "['name']", 'object_name': 'Database'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.department': {
            'Meta': {'ordering': "['name']", 'object_name': 'Department'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.informationsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'InformationSystem'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.AplicationServer']"}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']"}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']"}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'})
        },
        u'schemais.license': {
            'Meta': {'ordering': "['name']", 'object_name': 'License'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.operatingsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'OperatingSystem'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.partis': {
            'Meta': {'ordering': "['name']", 'object_name': 'PartIS'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'agenda': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Agenda']"}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.AplicationServer']"}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']"}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']"}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'information_system': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.InformationSystem']"}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'previous': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'previous_part_set'", 'to': u"orm['schemais.PartIS']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'})
        },
        u'schemais.pricing': {
            'Meta': {'ordering': "['contract__name']", 'object_name': 'Pricing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'once': ('django.db.models.fields.IntegerField', [], {}),
            'period': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['schemais']