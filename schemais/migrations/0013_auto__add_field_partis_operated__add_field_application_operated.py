# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PartIS.operated'
        db.add_column(u'schemais_partis', 'operated',
                      self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Application.operated'
        db.add_column(u'schemais_application', 'operated',
                      self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PartIS.operated'
        db.delete_column(u'schemais_partis', 'operated')

        # Deleting field 'Application.operated'
        db.delete_column(u'schemais_application', 'operated')


    models = {
        u'schemais.agenda': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Agenda'},
            'comm_ext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'comm_int': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'ordering': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'}),
            'reason': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'requirement': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'}),
            'users_note': ('tinymce.models.HTMLField', [], {'blank': 'True'})
        },
        u'schemais.agendarelation': {
            'Meta': {'object_name': 'AgendaRelation'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'destinations_set'", 'to': u"orm['schemais.Agenda']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources_set'", 'to': u"orm['schemais.Agenda']"})
        },
        u'schemais.application': {
            'Meta': {'ordering': "['name']", 'object_name': 'Application'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.ApplicationServer']", 'null': 'True', 'blank': 'True'}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']", 'null': 'True', 'blank': 'True'}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']", 'null': 'True', 'blank': 'True'}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'information_system': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.InformationSystem']"}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']", 'null': 'True', 'blank': 'True'}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'operated': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'previous': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'previous_part_set'", 'null': 'True', 'to': u"orm['schemais.Application']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'schemais.applicationserver': {
            'Meta': {'ordering': "['name']", 'object_name': 'ApplicationServer'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.database': {
            'Meta': {'ordering': "['name']", 'object_name': 'Database'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.department': {
            'Meta': {'ordering': "['name']", 'object_name': 'Department'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.informationsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'InformationSystem'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.ApplicationServer']", 'null': 'True', 'blank': 'True'}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']", 'null': 'True', 'blank': 'True'}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']", 'null': 'True', 'blank': 'True'}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']", 'null': 'True', 'blank': 'True'}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'operated': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'previous': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'previous_part_set'", 'null': 'True', 'to': u"orm['schemais.InformationSystem']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'schemais.license': {
            'Meta': {'ordering': "['name']", 'object_name': 'License'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.operatingsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'OperatingSystem'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.partis': {
            'Meta': {'ordering': "['name']", 'object_name': 'PartIS'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'agenda': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Agenda']", 'blank': 'True'}),
            'application': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Application']"}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.ApplicationServer']", 'null': 'True', 'blank': 'True'}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']", 'null': 'True', 'blank': 'True'}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']", 'null': 'True', 'blank': 'True'}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']", 'null': 'True', 'blank': 'True'}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'operated': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'previous': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'previous_part_set'", 'null': 'True', 'to': u"orm['schemais.PartIS']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'schemais.pricing': {
            'Meta': {'ordering': "['license__name']", 'object_name': 'Pricing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'once': ('django.db.models.fields.IntegerField', [], {}),
            'period': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['schemais']