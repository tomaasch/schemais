# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Agenda.start'
        db.delete_column(u'schemais_agenda', 'start')

        # Adding field 'Agenda.date_from'
        db.add_column(u'schemais_agenda', 'date_from',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Agenda.start'
        db.add_column(u'schemais_agenda', 'start',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Agenda.date_from'
        db.delete_column(u'schemais_agenda', 'date_from')


    models = {
        u'schemais.agenda': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'Agenda'},
            'comm_ext': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'comm_int': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'ordering': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'}),
            'reason': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'requirement': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'})
        },
        u'schemais.agendarelation': {
            'Meta': {'object_name': 'AgendaRelation'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'destinations_set'", 'to': u"orm['schemais.Agenda']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources_set'", 'to': u"orm['schemais.Agenda']"}),
            'typ': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'schemais.aplicationserver': {
            'Meta': {'ordering': "['name']", 'object_name': 'AplicationServer'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.database': {
            'Meta': {'ordering': "['name']", 'object_name': 'Database'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.department': {
            'Meta': {'ordering': "['name']", 'object_name': 'Department'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.informationsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'InformationSystem'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.AplicationServer']"}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']"}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']"}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'})
        },
        u'schemais.license': {
            'Meta': {'ordering': "['name']", 'object_name': 'License'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.operatingsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'OperatingSystem'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'schemais.partis': {
            'Meta': {'ordering': "['name']", 'object_name': 'PartIS'},
            'access': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'administrator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'agenda': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Agenda']"}),
            'architecture_as': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.AplicationServer']"}),
            'architecture_db': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.Database']"}),
            'architecture_os': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.OperatingSystem']"}),
            'backup_app': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_data': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'backup_file': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'documentation_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'information_system': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.InformationSystem']"}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'location': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'location_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'previous': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'previous_part_set'", 'to': u"orm['schemais.PartIS']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'blank': 'True'})
        },
        u'schemais.pricing': {
            'Meta': {'ordering': "['contract__name']", 'object_name': 'Pricing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schemais.License']"}),
            'once': ('django.db.models.fields.IntegerField', [], {}),
            'period': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['schemais']
