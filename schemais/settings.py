# -*- coding: utf-8 -*- 
import os, socket

FORCE_SCRIPT_NAME = ''
LOCAL_DEV = (socket.gethostname() == 'kuroneko')

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
PROJECT_NAME = 'schemais'


ADMINS = (
	(u'Tomáš Benda', 'tomas@labs.cz'),
)

MANAGERS = ADMINS

if LOCAL_DEV:
	DATABASES = {
    	'default': {
        	'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
	        'NAME': 'schmeais.db',                      # Or path to database file if using sqlite3.
   		}
	}
	DEBUG = True
else:
	DATABASES = {
    	'default': {
        	'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
	        'NAME': 'inka',                      # Or path to database file if using sqlite3.
			'USER': 'inka',
			'PASSWORD': 'AhTesa1e',
   		}
	}
	DEBUG = False
	DEBUG = True

TEMPLATE_DEBUG = DEBUG


# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['schemais-tacr.labs.cz', 'schemaistacr.labs.cz', 'schemais.tacr.cz', '127.0.0.1', 'localhost',]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Prague'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'cs'

SITE_ID = 2

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(os.path.dirname(PROJECT_DIR), '_media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(os.path.dirname(PROJECT_DIR), '_static/')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
	os.path.join(PROJECT_DIR, 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '~35LNKt)VLKw{=~qEwy1RVPbfGFr%n]Gs{{k{:|(@knA6t[#yv=8+QivxH_HC$2?FUu&HtE#w#3B*Q$guaJqR;[b0edX`52y:'


TINYMCE_DEFAULT_CONFIG = {
	'language': LANGUAGE_CODE,
	'width': 700,
	'height': 300,
	'skin': "o2k7",
	'skin_variant': "silver",
	'plugins': 'paste,fullscreen,preview,searchreplace,autolink,lists,table',
	'file_browser_callback': "CustomFileBrowser",
	'relative_urls': "false",
#   'plugin_preview_pageurl': reverse('tinymce-preview'),
	'table_inline_editing': True,
	'theme': "advanced",
	'theme_advanced_source_editor_width': 700,
	'theme_advanced_source_editor_height': 400,
	'theme_advanced_resizing': "true",
#   'theme_advanced_resizing_min_width': 700,
#   'theme_advanced_resizing_min_height': 300,
	'theme_advanced_toolbar_location': "top",
	'theme_advanced_statusbar_location': "bottom",
	'theme_advanced_buttons1': "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,copy,cut,paste,pastetext,pasteword,separator,removeformat,cleanup,code,|,undo,redo",
#   'theme_advanced_buttons2': "bullist,numlist,|,outdent,indent,|,link,unlink,|,hr,charmap,|,insertfile,insertimage,|,preview,|,search,replace,|,fullscreen",
#   'theme_advanced_buttons2': "bullist,numlist,|,outdent,indent,|,link,unlink,|,hr,charmap,|,preview,|,search,replace,|,fullscreen",
	'theme_advanced_buttons2': "bullist,numlist,|,outdent,indent,|,link,unlink,|,hr,charmap,|,search,replace,|,tablecontrols,|,fullscreen",
	'theme_advanced_buttons3': "",
#	'theme_advanced_fonts': 'Verdana',
#   'theme_advanced_font_sizes': "25px",
	'entity_encoding': "raw",
}


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
	'reversion.middleware.RevisionMiddleware',
	'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'schemais.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'schemais.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
	os.path.join(PROJECT_DIR, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
	'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.core.context_processors.static',
	'django.core.context_processors.tz',
	'django.contrib.messages.context_processors.messages',
	'social_auth.context_processors.social_auth_backends',
	'social_auth.context_processors.social_auth_login_redirect',
)

AUTHENTICATION_BACKENDS = (
	'social_auth.backends.google.GoogleOAuthBackend',
	'social_auth.backends.google.GoogleOAuth2Backend',
	'social_auth.backends.google.GoogleBackend',
	'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '595207058806-76d6mqjh6j2mnqbsd9du86n0sqvabr3i.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'dTixAI6BB0E79rVHTMveRo02'

INSTALLED_APPS = (
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.admin',
	'django.contrib.admindocs',
	'django.contrib.redirects',
	'south',
	'tinymce',
	'django_extensions',
	'reversion',
	'schemais',
	'references',
	'mptt',
	'treeadmin',
	'colors',
	'social_auth',
	'genericadmin',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
