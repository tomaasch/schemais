# -*- coding: utf-8 -*-

from django.db.models import F
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from models import *


@login_required
def graph_agenda(request):
#	agandas = get_list_or_404(Agenda)
#	raise Http404

	return render_to_response('graph_agenda.html', RequestContext(request, {
		'title': u'Graf agend',
		'description': u"""Zobrazení grafu agend a jejich návazností na moduly informačních systémů.<br/><br/>
		Kliknutím na uzel agendy přenesete tento uzel na střed grafu.<br/><br/>
		Každou agendu pak může vyřizovat jeden nebo více modulů IS.<br/><br/>
		Pordobnosti agendy (té, která je veprostřed grafu) jsou k dispoizci v pravém sloupci.
		V grafu jsou k agendám zobrazené jen aktivní softwarové prvky, tj. ty, které nemají status způsobu použití "nepoužívá se".
		""",
		'css_type': 'Hypertree',
		'nodes': Agenda.objects.all(),
#		'nodes': Agenda.objects.all().exclude(partis__status=70),
	}))


@login_required
def graph_is(request):

	return render_to_response('graph_is.html', RequestContext(request, {
		'title': u'Graf Informačních systémů',
		'description': u"""Zobrazení grafu informačních systémů s návazností na příslušné agendy (jsou až na konci stromu).<br/><br/>
		Kliknutím na uzel jej vyberte.<br/><br/>
		Kliknutím a popotáhnutím na plochu mimo uzle grafu můžete graf posunout.
		""",
		'css_type': 'Spacetree',
		'nodes': get_list_or_404(InformationSystem),
	}))


@login_required
def graph_relations(request):

	return render_to_response('graph_relations.html', RequestContext(request, {
		'title': u'Graf vazeb modulů informačních systémů',
		'description': u"""Zobrazení grafu vazeb modulů informačních systémů.<br/><br/>
		Kliknutím na jméno uzlu se vpravo zobrazí podrobnosti o napojení a v grafu se zvýrazní jeho vazby.<br/><br/>
		Kliknutím na 'X' uzel odmažete (jen v grafu, v db zůstane).<br/><br/>
		Rolovacím kolečkem můžte graf přibližovat a oddalovat.<br/><br/>
		""",
		'css_type': 'RGraph',
		'links': get_list_or_404(Link), #.order('source', 'destination'),
		'destinations': Link.objects.all().order_by('destination'),
	}))

