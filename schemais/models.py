# -*- coding: utf-8 -*-                                                                                                                                                                                                                                                                                                        
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from django.contrib.contenttypes.generic import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType

from tinymce import models as tinymce_models
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
import datetime
from colors.fields import ColorField

from references.models import *

#TODO:
# - zkontrolovat fieldy IS zda jsou v adminu
# - pridat field aplikacniho spravce
# - pridat m2n vazbu legislativa - agenda
# - pridat m2n vazbu agenda - informationsystem
# - prepsat grafovadla kvuli m2n

def url_to_edit_object(object):
	url = reverse('admin:%s_%s_change' %(object._meta.app_label,  object._meta.module_name),  args=[object.id] )
#	return u'<a href="%s">%s</a>' %(url,  object.__unicode__())
	return u'<a href="%s">%s</a>' %(url,  object.short_desc())

STATUS_CHOICES = (
	(10,	u'v provozu - plně se využívá',),
	(11,	u'v provozu - nevyužívá se zcela',),
	(20,	u'připravuje se',),
	(30,	u'zakoupeno - nenasazeno',),
	(40,	u'plánuje se',),
	(50,	u'archivační důvody',),
	(60,	u'nelze určit'),
	(70,	u'nepoužívá se'),
)

#AGENDA_RELATION_TYPE_CHOICES = (
#	(10,	u'vnitřní',),
#	(20,	u'vnější',),
#)

ACCESS_CHOICES = (
	(10,	u'webová aplikace',),
	(20,	u'terminálnový server Windows',),
	(30,	u'desktopová aplikace',),
	(40,	u'Android aplikace',),
	(50,	u'Datový sklad Oracle',),
)

OPERATED_CHOICES = (
	(10,	u'interně',),
	(20,	u'externě',),
	(30,	u'část interně, část externě',),
)

RELATION_CHOICES = (
	(10,	u'automaticky spouštěno službou'),
	(13,	u'přímé napojení ve stejné db'),
	(16,	u'přímé napojení v jiné db'),
	(20,	u'ručně spouštěno službou'),
	(25,	u'ruční export a následný import dat'),
	(30,	u'ruční přepis dat'),
	(50,	u'nedochází k přenosu potřebných dat'),
)

RELATION_FREQUENCY_CHOICES = (
	('online',		u'on-line'),
	('denne',		u'denně'),
	('tydne',		u'týdně'),
	('mesicne',		u'měsíčně'),
	('ctvrtletne',	u'čtvrtletně'),
	('pololetne',	u'pololetně'),
	('rocne',		u'ročně'),
	('adhoc',		u'ad hoc'),
#	('',	u''),
)

LEGISLATION_CHOICE = (
	(5,		u'Zřizovací listina',),
	(10,	u'Zákon',),
	(15,	u'Vyhláška',),
	(17,	u'Nařízení ministerstva',),
	(20,	u'Směrnice ministerstva',),
	(23,	u'Doporučení ministerstva',),
	(30,	u'Vnitřní směrnice',),
	(40,	u'Interní rozhodnutí',),
)

class Legislation(models.Model):
	name = models.CharField(_(u'Legislativní titul'), max_length=100)
	date_from = models.DateField(_(u'datum zavedení legislativního titulu'), blank=True, null=True)
#	status = models.PositiveIntegerField(_(u'Druh legislativního titulu'), choices=LEGISLATION_CHOICE)
	status = models.ForeignKey(Status_choice, verbose_name=_(u'Druh legislativního titulu'))
	description = tinymce_models.HTMLField(u'popis legislativního titulu', blank=True, help_text=u'Odkaz na zákon nebo směrnici')

	def __unicode__(self):
		return u'(%s) %s' % (self.get_status_display(), self.name)

	def short_desc(self):
		return u'%s' % self.name

	def agenda_links(self):
		links = [url_to_edit_object(agenda) for agenda in self.agenda_set.all()]
		if links == []:
			return u'<span style="color:#f00;font-weight:bold;">Nemá přiřazenu žádnou agendu</sapn>'
		return u'<li>'+u'<li>'.join(links)
	agenda_links.short_description = u'Legislativní titul je řešen agendami'
	agenda_links.allow_tags = True

	class Meta:
		verbose_name = u'Legislativní titul zavedeni agendy'
		verbose_name_plural = u'1. Legislativní tituly zavedeni agend'
		ordering = ['status', 'name']



class Agenda(MPTTModel):
#	legislation = models.ForeignKey(Legislation, verbose_name=_(u'Legislativní titul'), blank=True, null=True)
	legislation = models.ManyToManyField(Legislation, verbose_name=_(u'Legislativní titul'), blank=True, null=True)
#	parent = TreeForeignKey('self', verbose_name=_(u'Zastřešující agenda'), related_name='children', limit_choices_to={'parent': None,}, blank=True, null=True)
	parent = TreeForeignKey('self', verbose_name=_(u'Zastřešující agenda'), related_name='children', blank=True, null=True)
	name = models.CharField(_(u'jméno'), max_length=100)
#	ordering = models.PositiveIntegerField(_(u'číslo agendy v kruhu'), blank=True)
	description = tinymce_models.HTMLField(u'popis agendy', blank=True, help_text=u'co agenda zajišťuje')
#	status = models.PositiveIntegerField(_(u'aktuální stav agendy'), choices=STATUS_CHOICES, blank=True)
	reason = tinymce_models.HTMLField(u'důvod zavedení', blank=True, help_text=u'z čeho agenda vychází nebo legislativní vymezení')
	requirement = tinymce_models.HTMLField(u'požadavky na funkcionalitu', blank=True, help_text=u'co musí splňovat')
	comm_ext = tinymce_models.HTMLField(u'popis externí komunikace', blank=True, help_text=u'co s čím komunikuje')
	comm_int = tinymce_models.HTMLField(u'popis vnitřní komunikace', blank=True, help_text=u'co s čím komunikuje')
	date_from = models.DateField(_(u'datum zavedení agendy'), blank=True, null=True)
	users_note = tinymce_models.HTMLField(u'poznámka k počtu uživatelů', blank=True)
	note = tinymce_models.HTMLField(u'další poznámky', blank=True, help_text=u'poznámky, požadavky a návrhy na zlepšení')
#	color = models.CharField(_(u'barevné označení'), max_length=7, default='#ffffff')
	color = ColorField(_(u'barevné označení'), default='#ffffff', blank=True)

	def __unicode__(self):
		return self.name

	def short_desc(self):
		return u'%s' % self.name

	def legislation_link(self):
		if self.legislation == None:
			return u'<span style="color:#f00;font-weight:bold;">Není určen</sapn>'
		else:
			return url_to_edit_object(self.legislation)
#			url = reverse('admin:%s_%s_change' %(Legislation._meta.app_label,  Legislation._meta.module_name),  args=[self.legislation_id] )
#			return u'<a href="%s">%s</a>' %(url, self.legislation)
	legislation_link.short_description = u'Legislativní titul'
	legislation_link.allow_tags = True
	legislation_link.admin_order_field = 'legislation'

	def is_links(self):
		def striked(link, obj):
			if obj.status == 70:
				return u'<strike title="Nepoužívá se"><i>%s</i></strike>' % link
			return link

		links = [striked(url_to_edit_object(part_is), part_is) for part_is in self.partis_set.all()]
		if links == []:
			if self.is_leaf_node() and self.is_root_node():
				return u'<span style="color:#000;font-weight:bold;">Není řešeno v IS</sapn>'
			return u''
		return u'<li>'+u'<li>'.join(links)
	is_links.short_description = u'agenda je řešena v IS'
	is_links.allow_tags = True

	def show_color(self):
		return u'<div style="background-color:#%s; width:40px; height:15px;border:1px solid #000;"></div>' % self.color
	show_color.short_description = u'Barva'
	show_color.allow_tags = True

	class Meta:
		verbose_name = u'Agenda'
		verbose_name_plural = u'2. Agendy'
#		ordering = ['ordering']
		ordering = ['name']
	
	class MPTTMeta:
		order_insertion_by = ['name']



#class AgendaRelation(models.Model):
#	source = models.ForeignKey(Agenda, verbose_name=_(u'vstupní agenda'), related_name='sources_set')
#	destination = models.ForeignKey(Agenda, verbose_name=_(u'výstupní agenda'), related_name='destinations_set')
##	typ = models.PositiveIntegerField(_(u'typ komunikace'), choices=AGENDA_RELATION_TYPE_CHOICES)
#	description = tinymce_models.HTMLField(u'popis toku', blank=True)
#
#	def __unicode__(self):
#		return u'%s --> %s' % (self.source, self.destination)
#
#	class Meta:
#		verbose_name = u'Provázanost agend'
#		verbose_name_plural = u'2. Provázanosti agend'
#



#class Pricing(models.Model):
#	license = models.ForeignKey(License, verbose_name=_(u'smluvní vztah'))
#	once = models.IntegerField(_(u'jednorázová platba za pořízení'), help_text=u'v Kč')
#	period = models.IntegerField(_(u'cena pravidelné měsíční údržby'), help_text=u'v Kč')
#	note = models.CharField(_(u'poznámka'), max_length=250, blank=True)
#
#	def __unicode__(self):
#		return u'Cena za %s: pořízení: %s, měsíčně %s' % (self.license, self.once, self.period)
#
#	class Meta:
#		verbose_name = u'Cena smluvního vztahu'
#		verbose_name_plural = u'Ceny smluvních vztahů'
#		ordering = ['license__name']
#



class InformationSystem(MPTTModel):
	parent = TreeForeignKey('self', verbose_name=_(u'Nadřazený systém'), related_name='children', blank=True, null=True)
	name = models.CharField(_(u'označení IS'), max_length=100)
	code = models.CharField(_(u'zkratka IS'), max_length=100, blank=True)
	description = tinymce_models.HTMLField(u'popis aplikace', blank=True)
	location = tinymce_models.HTMLField(u'popis umístění', blank=True, help_text=u'kde běží')
	location_url = models.URLField(_(u'URL umístění'), max_length=200, blank=True)
	documentation = tinymce_models.HTMLField(u'popis umístění dokumentace', blank=True)
	documentation_url = models.URLField(_(u'URL dokumentace'), max_length=200, blank=True)
	previous = models.ForeignKey('self', related_name='previous_part_set', verbose_name=_(u'předchůdce IS'), blank=True, null=True)
	date_from = models.DateField(_(u'používá se od'), blank=True, null=True)
	date_to = models.DateField(_(u'používal se do'), blank=True, null=True)
#	status = models.PositiveIntegerField(_(u'způsob použití'), choices=STATUS_CHOICES, blank=True, null=True)
	status = models.ForeignKey(Status_choice, verbose_name=_(u'způsob použití'), blank=True, null=True)
#	agenda = TreeForeignKey(Agenda, verbose_name=_(u'řeší agendu'), blank=True)
	agenda = TreeManyToManyField(Agenda, verbose_name=_(u'řeší agendu'), blank=True, null=True)
	note = tinymce_models.HTMLField(u'poznámky k důvodům existence', blank=True)
	administrator = models.CharField(_(u'IT správce'), max_length=100, blank=True)
	app_administrator = models.CharField(_(u'aplikační správce'), max_length=100, blank=True)
	business_owner = models.CharField(_(u'business vlastník'), max_length=100, blank=True)
	license = models.ForeignKey(License, verbose_name=_(u'licence/smluvní vztah'), blank=True, null=True)
	architecture_os = models.ForeignKey(OperatingSystem, verbose_name=_(u'Operační systém'), blank=True, null=True)
	architecture_as = models.ForeignKey(ApplicationServer, verbose_name=_(u'Aplikační server'), blank=True, null=True)
	architecture_db = models.ForeignKey(Database, verbose_name=_(u'Databáze'), blank=True, null=True)
	backup_app = models.CharField(_(u'zálohování aplikace'), max_length=255, blank=True)
	backup_data = models.CharField(_(u'zálohování dat'), max_length=255, blank=True)
	backup_file = models.CharField(_(u'zálohování souborů'), max_length=255, blank=True)
#	access = models.PositiveIntegerField(_(u'způsob přístupu'), choices=ACCESS_CHOICES, blank=True, null=True)
	access = models.ForeignKey(Access_choice, verbose_name=_(u'způsob přístupu'), blank=True, null=True)
#	operated = models.PositiveIntegerField(_(u'porovzováno'), choices=OPERATED_CHOICES)
	operated = models.ForeignKey(Operated_choice, verbose_name=_(u'provozováno'), blank=True, null=True)
	color = ColorField(_(u'barevné označení'), default='ffffff', blank=True)
#	improvement = tinymce_models.HTMLField(u'návrh na zlepšení', blank=True, help_text=u'Popis problematických míst modulu a návrh na zlepšení')

	def __unicode__(self):
		return self.name

	def show_color(self):
		return u'<div style="background-color:#%s; width:40px; height:15px;border:1px solid #000;"></div>' % self.color
	show_color.short_description = u'Barva'
	show_color.allow_tags = True

	def agenda_link(self):
		if self.agenda == None:
			return u'<span style="color:#f00;font-weight:bold;">Není určen</sapn>'
		else:
			return url_to_edit_object(self.agenda)
	agenda_link.short_description = u'řeší agendu'
	agenda_link.allow_tags = True
	agenda_link.admin_order_field = 'agenda'

	class Meta:
		verbose_name = u'Informační systém'
		verbose_name_plural = u'3. Informační systémy'
		ordering = ['name']



class Link(models.Model):
	source = TreeForeignKey(InformationSystem, verbose_name=_(u'vstupní IS'), related_name='sources_set')
	destination = TreeForeignKey(InformationSystem, verbose_name=_(u'cílový IS'), related_name='destinations_set')
#	typ = models.PositiveIntegerField(_(u'typ přenosu dat'), choices=RELATION_CHOICES)
	typ = models.ForeignKey(Relation_choice, verbose_name=_(u'typ přenosu dat'))
#	frequency = models.CharField(u'Četnost přenosu dat', max_length=100, choices=RELATION_FREQUENCY_CHOICES, blank=True)
	frequency = models.ForeignKey(RelationFrequency_choice, verbose_name=_(u'Četnost přenosu dat'))
	note = models.CharField(u'stručný popis', max_length=100, blank=True)
	description = tinymce_models.HTMLField(u'popis toku', blank=True)
	improvement = tinymce_models.HTMLField(u'návrh na zlepšení', blank=True)

	def __unicode__(self):
		#return u'(%s) %s --> (%s) %s' % (self.source.name, self.source.name, self.destination.name, self.destination.name)
		return u'%s --> %s' % (self.source.name, self.destination.name)

	def frequency_status(self):
		if self.frequency:
#			return self.get_frequency_display()
			return self.frequency
		else:
			return u'<span style="color:#f00;font-weight:bold;">Není určeno</sapn>'
	frequency_status.short_description = u'Četnost přenosu dat'
	frequency_status.allow_tags = True
	frequency_status.admin_order_field = 'frequency'

	def typ_status(self):
		if self.typ.highlight:
			p1, p2 = u'<b>', u'</b>'

#		colors = {
#					10:	'#0f0', # online
#					13:	'#5d5', #přímé napojení ve stejné db'),
#					16:	'#6b6', #přímé napojení v jiné db'),
#					20:	'#000', #ručně spouštěno službou'),
#					25:	'#800', #ruční export a následný import dat'),
#					30:	'#d11', #ruční přepis dat'),
#					50:	'#f00', #nedochází k přenosu potřebných dat'),
#		}
#		if self.typ == 50:
#			p1, p2 = u'<b>!!! ', u' !!!</b>'
#		elif self.typ == 30:
#			p1, p2 = u'<b>', u'</b>'
		else:
			p1, p2 = u'', u''
		return u'<span style="color:%s;">%s%s%s</sapn>' % (self.typ.color, p1, self.typ, p2)
	typ_status.short_description = u'typ přenosu dat'
	typ_status.allow_tags = True
	typ_status.admin_order_field = 'typ'

	def have_improvement(self):
		if self.improvement:
			return True
		return False
	have_improvement.short_description = u'návrh na zlepšení'
	have_improvement.boolean = True

	class Meta:
		verbose_name = u'Vazba mezi moduly informačních systémů'
		verbose_name_plural = u'4. Vazby mezi moduly informačních systémů'
		ordering = ['source', 'destination']


class Users(models.Model):
	information_system = models.ForeignKey(InformationSystem, verbose_name=_(u'modul IS'))
	deparment = models.ForeignKey(Department, verbose_name=_(u'oddělení'))
	number = models.PositiveIntegerField(u'Počet uživatelů', blank=True, null=True)
	position = models.CharField(u'funkce', max_length=100, blank=True)



class Problem(models.Model):
	name = models.CharField(_(u'pojmenování problému'), max_length=100)
	description = tinymce_models.HTMLField(u'popis problému', blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Problém k řešení'
		verbose_name_plural = u'5. Problémy k řešení'



class ProblemRelation(models.Model):
	relations_limits = {'model__in': ('legislation', 'agenda', 'informationsystem', 'link',)}

	problem = models.ForeignKey(Problem, verbose_name=_(u'problém'))
	content_type = models.ForeignKey(ContentType, limit_choices_to=relations_limits, verbose_name=u'typ souvislosti')
	object_id = models.PositiveIntegerField(u'identifikátor objektu')
	content_object = GenericForeignKey('content_type', 'object_id')

	def get_object(self):
		ct = ContentType.objects.get_for_id(self.content_type)
		obj = ct.get_object_for_this_type(pk=self.object_id)
		return obj

	class Meta:
		verbose_name = u'Souvislost'
		verbose_name_plural = u'Souvislosti'
#class Supplier




