# -*- coding: utf-8 -*-
#from django.conf.urls.defaults import *
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings


from views import graph_agenda, graph_is, graph_relations


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'inka.views.home', name='home'),
    # url(r'^inka/', include('inka.foo.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
	url(r'^tinymce/', include('tinymce.urls')),
	url(r'^graf-agend/', graph_agenda, name='graph_agenda'),
	url(r'^graf-is/', graph_is, name='graph_is'),
	url(r'^graf-vazeb/', graph_relations, name='graph_relations'),
	url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'admin/login.html'}),
	url(r'', include('social_auth.urls')),
)

if settings.DEBUG:
	urlpatterns = patterns('',
		url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
			{'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
		url(r'', include('django.contrib.staticfiles.urls')),
	) + urlpatterns
