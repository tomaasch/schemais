# -*- coding: utf-8 -*- 
from django.contrib import admin
from django.template.defaultfilters import slugify
from models import *
#from django.conf import settings
import reversion
from treeadmin.admin import TreeAdmin


#class PricingInline(admin.TabularInline):
#	model = Pricing

class Status_choiceAdmin(reversion.VersionAdmin):
	list_display = ['name', 'highlight']

class Operated_choiceAdmin(reversion.VersionAdmin):
	list_display = ['name']

class Access_choiceAdmin(reversion.VersionAdmin):
	list_display = ['name']

class Relation_choiceAdmin(reversion.VersionAdmin):
	list_display = ['name', 'show_color', 'highlight']
	fieldsets = (
		(None, {
			'fields': (
				'name', ('highlight', 'color'),
			)
		}),
	)

class RelationFrequency_choiceAdmin(reversion.VersionAdmin):
	list_display = ['name', 'show_color', 'order']
	fieldsets = (
		(None, {
			'fields': (
				'name', ('order', 'color',),
			)
		}),
	)

class Legislation_choiceAdmin(reversion.VersionAdmin):
	list_display = ['name']

class LicenseAdmin(reversion.VersionAdmin):
	list_display = ['name']
	search_fields = ['description']
#	inlines = [PricingInline]

class OperatingSystemAdmin(reversion.VersionAdmin):
	list_display = ['name']
	search_fields = ['description']

class ApplicationServerAdmin(reversion.VersionAdmin):
	list_display = ['name']
	search_fields = ['description']

class DatabaseAdmin(reversion.VersionAdmin):
	list_display = ['name']
	search_fields = ['description']

class DepartmentAdmin(reversion.VersionAdmin):
	list_display = ['name']
	search_fields = ['description']

class PricingAdmin(reversion.VersionAdmin):
	fieldsets = (
		(None, {
			'fields': (
				'license', ('once', 'period',), 'note'
			)
		}),
	)
	list_display = ['license', 'once', 'period']


admin.site.register(Status_choice, Status_choiceAdmin)
admin.site.register(Operated_choice, Operated_choiceAdmin)
admin.site.register(Access_choice, Access_choiceAdmin)
admin.site.register(Relation_choice, Relation_choiceAdmin)
admin.site.register(RelationFrequency_choice, RelationFrequency_choiceAdmin)
admin.site.register(Legislation_choice, Legislation_choiceAdmin)
admin.site.register(License, LicenseAdmin)
#admin.site.register(Pricing, PricingAdmin)
admin.site.register(OperatingSystem, OperatingSystemAdmin)
admin.site.register(ApplicationServer, ApplicationServerAdmin)
admin.site.register(Database, DatabaseAdmin)
admin.site.register(Department, DepartmentAdmin)

class Admin(reversion.VersionAdmin):
	fieldsets = (
		(None, {
			'fields': (
			)
		}),
	)

