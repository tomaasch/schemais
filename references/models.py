# -*- coding: utf-8 -*-                                                                                                                                                                                                                                                                                                        
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from tinymce import models as tinymce_models
#from mptt.models import MPTTModel, TreeForeignKey
import datetime
from colors.fields import ColorField

def url_to_edit_object(object):
	url = reverse('admin:%s_%s_change' %(object._meta.app_label,  object._meta.module_name),  args=[object.id] )
#	return u'<a href="%s">%s</a>' %(url,  object.__unicode__())
	return u'<a href="%s">%s</a>' %(url,  object.short_desc())



class Status_choice(models.Model):
	name = models.CharField(_(u'způsob využití'), max_length=100)
	highlight = models.BooleanField(_(u'zvýraznění'), default=False)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Způsob využití'
		verbose_name_plural = u'Způsoby využití IS'
		ordering = ['id']



class Operated_choice(models.Model):
	name = models.CharField(_(u'druh provozu'), max_length=100)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Druh provozu'
		verbose_name_plural = u'Druhy provozu IS'
		ordering = ['id']



class Access_choice(models.Model):
	name = models.CharField(_(u'způsob přístupu'), max_length=100)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Způsob přístupu'
		verbose_name_plural = u'Způsoby přístupů k IS'
		ordering = ['id']



class Relation_choice(models.Model):
	name = models.CharField(_(u'Typ vazby'), max_length=100)
	color = ColorField(_(u'barevné označení'), default='ffffff', blank=True)
	highlight = models.BooleanField(_(u'zvýraznění'), default=False)

	def __unicode__(self):
		return self.name

	def show_color(self):
		return u'<div style="background-color:#%s; width:40px; height:15px;border:1px solid #000;"></div>' % self.color
	show_color.short_description = u'Barva'
	show_color.allow_tags = True

	class Meta:
		verbose_name = u'Druh vazby mezi IS'
		verbose_name_plural = u'Druhy vazeb mezi IS'
		ordering = ['id']



class RelationFrequency_choice(models.Model):
	name = models.CharField(_(u'označení četnosti přenosu dat'), max_length=100)
	color = ColorField(_(u'barevné označení'), default='ffffff', blank=True)
	order = models.PositiveSmallIntegerField(_(u'pořadí'), default=10)

	def __unicode__(self):
		return self.name

	def show_color(self):
		return u'<div style="background-color:#%s; width:40px; height:15px;border:1px solid #000;"></div>' % self.color
	show_color.short_description = u'Barva'
	show_color.allow_tags = True

	class Meta:
		verbose_name = u'Četnost přenosu dat'
		verbose_name_plural = u'Četnosti přenosů dat'
		ordering = ['order']



class Legislation_choice(models.Model):
	name = models.CharField(_(u'jméno legislativního titulu'), max_length=100)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Legislativní titul'
		verbose_name_plural = u'Legislativní tituly'
		ordering = ['id']



class License(models.Model):
	name = models.CharField(_(u'jméno'), max_length=100)
	description = tinymce_models.HTMLField(u'popis smluvního vztahu', blank=True, help_text=u'smluvní vztah či licence')

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Smluvní vztah/licence'
		verbose_name_plural = u'Smluvní vztahy/licence'
		ordering = ['name']



class OperatingSystem(models.Model):
	name = models.CharField(_(u'jméno operačního systému'), max_length=100)
	description = tinymce_models.HTMLField(u'popis OS', blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Operační systém'
		verbose_name_plural = u'Operační systémy'
		ordering = ['name']



class ApplicationServer(models.Model):
	name = models.CharField(_(u'aplikační server'), max_length=100)
	description = tinymce_models.HTMLField(u'popis aplikačního serveru', blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Aplikační server'
		verbose_name_plural = u'Aplikační servery'
		ordering = ['name']



class Database(models.Model):
	name = models.CharField(_(u'databáze'), max_length=100)
	description = tinymce_models.HTMLField(u'popis databáze', blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Databáze'
		verbose_name_plural = u'Databáze'
		ordering = ['name']



class Department(models.Model):
	name = models.CharField(_(u'označení vnitřní organizační jednotky'), max_length=100)
	description = tinymce_models.HTMLField(u'poznámka', blank=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = u'Vnitřní organizační jednotka'
		verbose_name_plural = u'Vnitřní organizační jednotky'
		ordering = ['name']


